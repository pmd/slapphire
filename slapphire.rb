#!/usr/bin/ruby
# Slackware package / package info downloader
require 'net/ftp'
require 'fileutils'

# Target repository
# Please be careful with this - a wrong version/server can mess up your
# system with incompatibile or even fraudulent packages. 
ftp_url = 'slackware.org.uk'
version = 'slackware-14.1'

# Keep a flat text file list of all the available files (packages and desc)
# Fetch the key from the repository and add it to root's keyring
def update_db(url, version)
  #Always clear before downloading
  db = File.open(ENV['HOME'] + '/.slapphire/package_db', 'w+')
  db.truncate(0)

  ftp = Net::FTP.new(url)
  ftp.login
  
################################################################################
  # Move into the version directory to fetch the GPG key
  # If it's already there - add it anyway.
  ftp.chdir("slackware/#{version}")
  ftp.getbinaryfile('GPG-KEY', '/tmp/GPG-KEY')
  system "gpg --import /tmp/GPG-KEY"
  FileUtils.rm('/tmp/GPG-KEY')
################################################################################
  
  # Move into the package index directory
  ftp.chdir('slackware')
  p_ar = ftp.nlst('*')
  ftp.close
  
  # Arrange the list so 0 =>.txt,  1 => .txz,  2 =>.txz.asc
  p_ar = p_ar.sort
  
  # We don't want .txz.asc files. We know that there is always one there.
  p_ar.each do |pkg|
    if pkg.include?('txz')
      db.write(pkg + "\n")
    elsif pkg.include?('txt')
      db.write(pkg + "\n")
    end
  end
  
end

def install_pkg(pkg_name, ftp_url, version)
  db = File.open(ENV['HOME'] + '/.slapphire/package_db', 'r')
  # We want an array so we can get specific filetypes (refer above)
  results = Array.new

  # Go through the list and pick out matches
  db.each do |file|
    if file.include?(pkg_name)
      results << file
    end
  end
  
  # If we have 0 results, it isn't in out database
  # If we have more than one package (3 files p/pkg) we have too many results
  if (results.length == 0)
    puts "Package '#{pkg_name}' not found"
    
  elsif (results.length > 3)
    puts "Multiple matches found for #{pkg_name}:"
    results.each do | result | 
      if result.include?('.txz') and !result.include?('.txz.asc')
	# Print out the full package name (everything after the slash)
	puts (result.partition("/")[2])
      end
    end
  else 
    
    puts "Connecting to #{ftp_url}\n"
    ftp = Net::FTP.new(ftp_url)
    ftp.login
    ftp.chdir("slackware/#{version}/slackware")
    
    # Bite off the newline character
    package = results[1].chomp
    package_target = "/tmp/" + (package.partition("/")[2])
    
    # Get the size so we can estimate download progress
    package_size = ftp.size(package)
    package_down = 0
    
    # Pass through the chunks, tick them off until they add up to package_size
    ftp.getbinaryfile(package, package_target, 1024) do |data|
      package_down += data.size
      percent_finished = ((package_down.to_f / package_size.to_f) *100)
      print "#{package_target} => #{percent_finished.round}%\r"
    end
    
    print "\n\n"
    
################################################################################
    
    # Comment out this to disable signature checking
    ftp.gettextfile((package + '.asc'), (package_target + '.asc'))
    if (system "gpg --with-fingerprint #{package_target + '.asc'} 1>/dev/null 2>/dev/null")
      puts "Good signature, installing package!\n\n"
      # Run installpkg on verified package
      system "installpkg #{package_target}"
      FileUtils.rm(package_target + '.asc')
    else
      puts "Bad signature! not installing!"
      FileUtils.rm(package_target + '.asc')
    end
################################################################################
    
    # If you did comment out signature checking - make sure you uncomment this!
    # system "installpkg #{package_target}"

################################################################################
    
    # Close ftp
    ftp.close
    
    # Edit this out if you don't want to delete packages after installing them.
    puts ("Cleaning up #{package_target}")
    FileUtils.rm(package_target)
  end
end

# If the database isn't there, we need to force an update.
if !(File.directory?(ENV['HOME'] + '/.slapphire'))
  puts 'Missing ~/.slapphire - creating directory and fetching DB'
  Dir.mkdir(ENV['HOME'] + '/.slapphire')
  update_db(ftp_url, version)
end

begin
  # Manually force an update
  if (ARGV[0] == "up" || ARGV[0] == "-u")
    puts "Fetching package list from #{ftp_url}"
    update_db(ftp_url, version)
  # Install package
  elsif ((ARGV[0] == "in" || ARGV[0] == "-i") && !(ARGV[1].nil?))
    ARGV.shift
    ARGV.each do |pkg|
      install_pkg(pkg, ftp_url, version)
      puts ""
    end
  else
    puts 'Usage:'
    puts 'slapphire in (-i) <packagename> [<packagename>]'
    puts 'slapphire up (-u)'
  end
  
rescue Interrupt
  puts "\nOkay, okay. Exiting immediately!\n"
rescue SocketError
  puts "Connection error. Check your network connection."
end

