# Slapphire
Slapphire is a wrapper for installpkg that adds FTP fetching functionality. It 
takes package names and fetches them from a slackware FTP. You can specify
what FTP, and even what version. Although, slackware.org.uk and 14.0 are the
default. On top of this, it also checks to see if the package is correctly
signed. However! Slapphire fetches the GPG key from the server and adds it to 
the root user's GPG keyring. If you do not want this - just edit out the lines
in update_db() and modify install_pkg() - both are marked clearly. 

## Depends on:
ruby >= 1.8  
slackware package utilities (installpkg, removepkg)  

## Usage (As root):
`slapphire in (-i) <package_name> [additional_package_name]`

`slapphire up (-u)`  


## Note:
Matching is not complete. 'vim' will not install vim - instead it will return
matches for 'vim-7.3.645-i486-1.txz' and 'vim-gvim-7.3.645-i486-1.txz'. 'vim'
should be extended to 'vim-7.3.645-i486-1.txz' - or just 'vim-7.3' for now. 
Being specific works. This will be fixed as slapphire moves away from 
String.include?().

## Todo:
1. Move away from String.include?().
2. Restructure to use single FTP connection for multiple packages.


